package com.app.app;


import com.app.app.proto.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class App {

    private static JFrame frame;
    public static MedicationGrpc.MedicationBlockingStub stub;
    public static int medSize = 0;

    public static void main(String[] args) throws Exception {

       ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",9090).usePlaintext().build();

       stub = MedicationGrpc.newBlockingStub(channel);

       Empty empty = Empty.newBuilder().build();

        String intake_interval;
        boolean firstDownload = false;

        while(true){
            Calendar calendar = new GregorianCalendar();
            int hour = calendar.get( Calendar.HOUR );
            int minute = calendar.get( Calendar.MINUTE );
            int seconds = calendar.get(Calendar.SECOND);
//            if( calendar.get( Calendar.AM_PM ) == 0 ){
//                //AM
//            }else{
//                //PM
//            }

            if ((hour == 0 && minute == 0 && seconds == 0 && calendar.get( Calendar.AM_PM ) == Calendar.AM) || !firstDownload){
                    MedicationList response = stub.sendMedication(empty);
                    ArrayList<String> listMed = new ArrayList<>();
                    for (med m: response.getMedList()){
                        System.out.println(m.getMedName());
                        listMed.add(m.getMedName());
                    }
                    medSize = listMed.size()-1;
                    intake_interval = response.getIntake();
                    System.out.println("Intake: " + intake_interval);
                    View view = new View(listMed, intake_interval);
                    firstDownload = true;
                }


        }
    }
}
