package com.app.app;

import com.app.app.proto.MedicationID;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class View {


    public static boolean allTaken = false;

    public View(ArrayList<String> listMed, String intake_interval){

        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();

        panel1.setLayout(new GridLayout(1,2));
        JLabel timer1 = new JLabel();
        Timer timer = new Timer();

        TimerTask task = new TimerTask() {
            private final int MAX_SECONDS = Integer.parseInt(intake_interval) * 3600;
            int seconds = 0;
            int hours = Integer.parseInt(intake_interval) , mins = 60, sec = 60;

            @Override
            public void run() {
                if (seconds < MAX_SECONDS) {
                    //System.out.println("Seconds = " + seconds);
                    seconds++;
                    int countDown = MAX_SECONDS - seconds;

                    if(hours == Integer.parseInt(intake_interval)){
                        hours--;
                        mins=59;
                        sec=60;
                    }
                    sec--;
                    if (sec == 0){
                        mins--;
                        sec = 59;
                    }
                    if(mins == 0){
                        hours--;
                        mins= 59;
                    }

                    timer1.setText("Timer: " +  hours +":"+mins+":"+sec);
                    timer1.repaint();
                } else {
                    // stop the timer
                    cancel();
                }

            }
        };

        timer.schedule(task,0,1000);

        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm ");
        Date date = new Date(System.currentTimeMillis());
        JLabel ora = new JLabel(formatter.format(date));

        JFrame frame = new JFrame("Pill Dispenser");
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.setSize(500, 400);

        //Tabel Medicamente
        JTable table1 = createTable(listMed);
        JScrollPane pane=new JScrollPane(table1);
        frame.getContentPane().add(pane);

        panel1.add(ora);
        panel1.add(timer1);
        panel2.add(pane);

        frame.add(panel1, BorderLayout.NORTH);
        frame.add(panel2, BorderLayout.CENTER);
        frame.setVisible(true);
    }


    public <T> JTable createTable(ArrayList<String> list) {
        JTable tab = new JTable();

        DefaultTableModel model = new DefaultTableModel(
                new Object[] { "Nume Medicament", "Action" }, 0);
        for (String nume: list) {
                model.addRow(new Object[] {nume, "Take"});
        }
        tab = new JTable(model);
        tab.getColumnModel().getColumn(1).setCellRenderer(new ButtonRenderer());;

        //SET CUSTOM EDITOR TO TEAMS COLUMN
        tab.getColumnModel().getColumn(1).setCellEditor(new ButtonEditor(new JTextField()));

        return tab;
    }


    class ButtonRenderer extends JButton implements TableCellRenderer
    {

        //CONSTRUCTOR
        public ButtonRenderer() {
            //SET BUTTON PROPERTIES
            setOpaque(true);
        }
        @Override
        public Component getTableCellRendererComponent(JTable table, Object obj,
                                                       boolean selected, boolean focused, int row, int col) {

            //SET PASSED OBJECT AS BUTTON TEXT
            setText((obj==null) ? "":obj.toString());

            return this;
        }

    }

    //BUTTON EDITOR CLASS
    class ButtonEditor extends DefaultCellEditor {
        protected JButton btn;
        private String lbl;
        private Boolean clicked;
        private Object medication;
        private JTable table;
        private int row;

        public ButtonEditor(JTextField txt) {
            super(txt);

            btn = new JButton();
            btn.setOpaque(true);

            //WHEN BUTTON IS CLICKED
            btn.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    if (table != null) {
                        fireEditingStopped();
                        TableModel model = table.getModel();
                        if (model instanceof DefaultTableModel) {
                            ((DefaultTableModel) model).removeRow(row);
                        }
                    }
//                    if(table.getModel().getRowCount() == 0){
//                        allTaken = true;
//                    }
                }
            });

        }

        //OVERRIDE A COUPLE OF METHODS
        @Override
        public Component getTableCellEditorComponent(JTable table, Object obj,
                                                     boolean selected, int row, int col) {

            //SET TEXT TO BUTTON,SET CLICKED TO TRUE,THEN RETURN THE BTN OBJECT
            medication = table.getModel().getValueAt(row,col-1);
            lbl = (obj == null) ? "" : obj.toString();
            btn.setText(lbl);
            clicked = true;
            this.table = table;
            this.row = row;
            return btn;
        }

        //IF BUTTON CELL VALUE CHNAGES,IF CLICKED THAT IS
        @Override
        public Object getCellEditorValue() {

            if (clicked) {
                //SHOW US SOME MESSAGE
                System.out.println(App.medSize);
                MedicationID all = MedicationID.newBuilder().setAllMedication(medication.toString()).setTotalMed(String.valueOf(App.medSize--)).build();
                App.stub.medicationTaken(all);
                System.out.println("Taken: " + medication);

            }
            //SET IT TO FALSE NOW THAT ITS CLICKED
            clicked = false;
            return new String(lbl);
        }

        @Override
        public boolean stopCellEditing() {

            //SET CLICKED TO FALSE FIRST
            clicked = false;
            return super.stopCellEditing();
        }

        @Override
        protected void fireEditingStopped() {
            // TODO Auto-generated method stub
            super.fireEditingStopped();
        }
    }
}
