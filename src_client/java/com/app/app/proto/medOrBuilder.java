// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: scratch.proto

package com.app.app.proto;

public interface medOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.app.app.proto.med)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string medName = 1;</code>
   */
  java.lang.String getMedName();
  /**
   * <code>string medName = 1;</code>
   */
  com.google.protobuf.ByteString
      getMedNameBytes();
}
