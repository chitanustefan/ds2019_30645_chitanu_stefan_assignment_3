package com.app.app;

import com.app.app.proto.*;
import io.grpc.stub.StreamObserver;

public class MedProtoService extends MedicationGrpc.MedicationImplBase {



    @Override
    public void sendMedication(Empty request, StreamObserver<MedicationList> responseObserver) {
        System.out.println("ALL MEDICATION LIST");

        MedicationList.Builder response = MedicationList.newBuilder();

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();

    }

    @Override
    public void medicationTaken(MedicationID request, StreamObserver<MedicationResponse> responseObserver) {
        System.out.println("Medication");

        MedicationResponse.Builder response = MedicationResponse.newBuilder();
        String allTaken = request.getAllMedication();
        System.out.println(allTaken);
        if(allTaken.equals("1")){
            response.setMessage("Toate medicamentele au fost luate");
        }else{
            response.setMessage("Mai trebuie luate medicamente");
        }


        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }
}
