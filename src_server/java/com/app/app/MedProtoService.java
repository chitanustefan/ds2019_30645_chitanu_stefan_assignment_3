package com.app.app;

import com.app.app.entities.MedicationPerPlan;
import com.app.app.entities.MedicationPlan;
import com.app.app.services.MedicationPerPlanService;
import com.app.app.services.MedicationPlanService;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MedProtoService
        extends MedicationGrpc.MedicationImplBase {


    @Override
    public void sendMedication(Empty request, StreamObserver<MedicationList> responseObserver) {
        System.out.println("ALL MEDICATION LIST");

        MedicationList.Builder response = MedicationList.newBuilder();
        med.Builder med = com.app.app.med.newBuilder();

        for (String medication: com.app.app.gRPCServer.medicationList){
            med.setMedName(medication);
            response.addMed(med);
            response.setIntake(gRPCServer.intake_interval);
        }

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void medicationTaken(MedicationID request, StreamObserver<MedicationResponse> responseObserver) {

        MedicationResponse.Builder response = MedicationResponse.newBuilder();
        String medicationTaken = request.getAllMedication();
        System.out.println("Pacient took " + medicationTaken);
        if(Integer.parseInt(request.getTotalMed())==0){
            response.setMessage("Pacient took all medicine");
            System.out.println("Pacient took all medicine");
        }

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }
}
