package com.app.app.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@Table(name = "monitored_data2")
public class MonitoredData2 {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id_data",unique = true, nullable = false)
    private Long id_data;

    @Column
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime startTime;

    @Column
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime finishTime;

    @Column
    String act;

    @Column
    private int id_patient;

    public MonitoredData2() {
    }

    public MonitoredData2(LocalDateTime date, LocalDateTime finishTime, String act) {
        this.startTime = date;
        this.finishTime = finishTime;
        this.act = act;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(LocalDateTime finishTime) {
        this.finishTime = finishTime;
    }

    public String getAct() {
        return act;
    }

    public void setAct(String act) {
        this.act = act;
    }

    public Duration getDuration() {
        Duration duration = Duration.between(this.getStartTime(), this.finishTime);
        return duration;
    }

    public Long getId_data() {
        return id_data;
    }

    public void setId_data(Long id_data) {
        this.id_data = id_data;
    }

    public int getId_patient() {
        return id_patient;
    }

    public void setId_patient(int id_patient) {
        this.id_patient = id_patient;
    }
}
