package com.app.app.controller;

import com.app.app.entities.Medication;
import com.app.app.entities.MedicationPerPlan;
import com.app.app.entities.MedicationPlan;
import com.app.app.entities.Patient;
import com.app.app.services.MedicationPlanService;
import com.app.app.services.MedicationService;
import com.app.app.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class MedicationController {


    @Autowired
    MedicationPlanService medicationPlanService = new MedicationPlanService();

    @Autowired
    MedicationService medicationService = new MedicationService();

    @Autowired
    PatientService patientService = new PatientService();

    public MedicationController() {
    }

    @PostMapping("{id_patient}/{intake}/{period}/add/medplan")
    public MedicationPlan addMedicationPlan(@RequestBody List<Medication> medication, @PathVariable("id_patient") int id_patient,
                                            @PathVariable("intake") int intake, @PathVariable("period") int period)  {

        MedicationPlan medPlan = new MedicationPlan();
        System.out.println(id_patient);
        System.out.println(intake);
        System.out.println(period);
        for(Medication m : medication)
            System.out.println(m.getName());

        List<Patient > patientList = patientService.getAllPatient();
        Patient patient = new Patient();
        for(Patient p: patientList){
            if(p.getId_patient() == id_patient)
                patient = p;
        }
        List<MedicationPerPlan> mp = new ArrayList<>();

        for(Medication m: medication){
            MedicationPerPlan medperPlan = new MedicationPerPlan();
            medperPlan.setMedication(m);
            medperPlan.setMedicationPlan(medPlan);
            mp.add(medperPlan);
        }


        medPlan.setPatient(patient);
        medPlan.setIntake_intervals(Integer.toString(intake));
        medPlan.setPeriod(Integer.toString(period));
        medPlan.setMedicationPerPlans(mp);

        return medicationPlanService.saveMedicationPlan(medPlan);

    }

    @PostMapping("/doctor/medication-list")
    public Medication editMedication(@RequestBody Medication medication){
        System.out.println(medication.getName());
        return medicationService.updateMedication(medication.getId_medication(), medication);

    }

    @PostMapping("/delete/medication")
    public void deleteMedication(@RequestBody int id_medication) {
        System.out.println("Delete: " + id_medication);
        Long id = (long) id_medication;
        medicationService.deleteMedication(id);

    }

    @GetMapping("/doctor/medication-list")
    public List<Medication> getAllMedications(){
        List<Medication> list = medicationService.getAllMedications();
        return list;
    }

    @PostMapping("/add/medication")
    public Medication addMedication(@RequestBody Medication medication) throws ParseException {

        System.out.println(medication.getSideEffects());
        return  medicationService.saveMedication(medication);

    }

}
